class Prime
	@@range
	puts "Enter the range"
	@@range=gets.chomp
	puts "Showing prime numbers upto #{@range}"

	def method1
		@flag=1
		(2..@@range.to_i).each do |i|
			
			(2..(i/2).to_i).each do |j|
				@flag=1
				
				if i%j==0
					@flag=0
					break
				end
			end

			if @flag==1
				puts "#{i}"
			end
		end
	end

	def method2
		@flag=1
		for i in 2..@@range.to_i
			for j in 2..((i/2).to_i)
				@flag=1
				if i%j==0
					@flag=0
					break
				end
			end

			if @flag==1
				puts "#{i}"
			end
		end


	end
end


obj1=Prime.new
obj1.method1
puts "\n"
obj1.method2
