count = 6
case
when count==1
	puts 'single person'
when count == 2
	puts 'two person'
when (2..5).include?(count)
	puts 'several people'
else
	puts 'many people'
end